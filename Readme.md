CaspR Semi-Automatic Coreference Resolution Adjudication Tool based on Answer Set Programming (published at LPNMR 2017)

THIS REPOSITORY HAS BEEN MOVED TO https://github.com/knowlp/caspr-coreference-tool !

(Reason: missing Travis-CI support on bitbucket.com.)
